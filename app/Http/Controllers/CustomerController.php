<?php

namespace App\Http\Controllers;

use App\Repository\Eloquent\CustomerRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CustomerController extends Controller
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * CustomerController constructor.
     * @param CustomerRepository $customerRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $customers = $this->customerRepository->fetchCustomersReport();
        return view('customer.index', compact('customers'));
    }
}
