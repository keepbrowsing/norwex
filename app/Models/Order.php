<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    const TYPE_PENDING = 'pending';
    const TYPE_COMPLETED = 'completed';
    const TYPE_DELIVERED = 'delivered';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

//    /**
//     * @return BelongsTo
//     */
//    public function customer()
//    {
//        return $this->belongsTo(Customer::class, 'customer_id');
//    }
}
