<?php

namespace App\Repository\Eloquent;

use App\Models\Customer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CustomerRepository extends BaseRepository
{
    /**
     * CustomerRepository constructor.
     *
     * @param Customer $model
     */
    public function __construct(Customer $model)
    {
        parent::__construct($model);
    }

    /**
     * Fetch customer status and order report
     * @return Collection|null
     */
    public function fetchCustomersReport(): ?Collection
    {
        // Raw query has been used, but can be achieved with the eloquent query builder as well.
        // I was more comfortable with RAW to achieve particularly this record :)

        $rawQuery = "SELECT c.*,cs.name as status_name,cs.code as status_code,
         (select count(o.id) from orders o where o.customer_id = c.id) as total_orders,
         (select count(o.id) from orders o where o.customer_id = c.id
         AND c.status_id=(select c_st.id from customer_status c_st where c_st.code='AC')
         AND o.created_at > now() - INTERVAL 12 month) as total_orders_12_months,
         (select sum(o.order_total) from orders o where o.customer_id = c.id
         AND c.status_id=(select c_st.id from customer_status c_st where c_st.code='AC')
         AND o.order_status = '" . Order::TYPE_COMPLETED . "'
         AND o.created_at > now() - INTERVAL 3 month) as total_orders_3_months
         from customers c
        INNER JOIN customer_status cs ON cs.id = c.status_id";
        $customers = DB::select($rawQuery);
        return Collect($customers);
    }
}
