## Norwex Technical Challenge In Laravel 8

This is a demo test written to achieve the following.

- Highlight ‘Removed’ customers in RED
- ‘Active’ customers who have not placed any orders during the last 12 months in ORANGE
- ‘Active’ customers who have placed a minimum of RM200.00 in sales over the last 3 months in GREEN (check the ‘OrderStatus’ and make sure you are only including ‘Completed’ orders in this calculation)
- Include a total order count for each customer

## What has been implemented?
- Migration for database
- Factories to generate fake/sample data.
- Seeders to feed test data into the database(customers,orders and customer types)
- Repository to handle database related jobs.
- customer list page to achieve above mentioned requirements.

## Installation Guide
```
composer install
npm install
php artisan migrate:fresh --seed
```

## Notes
- Seeded working database has been attached , which makes easier to test.
- Homepage route (in this case i am using valet http://norwex.test/) will get the list of customers.
