<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create customer status
        $this->call([
            CustomerStatusSeeder::class,
        ]);

        // Create customers and orders
        Customer::factory()
            ->times(10)
            ->hasOrders(20)
            ->create();
    }
}
