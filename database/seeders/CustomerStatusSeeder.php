<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultStatus = [
            [
                'code' => 'AC',
                'name' => 'Active',
            ], [
                'code' => 'RE',
                'name' => 'Removed',
            ]
        ];

        foreach ($defaultStatus as $status) {
            DB::table('customer_status')->insertOrIgnore($status);
        }
    }
}
