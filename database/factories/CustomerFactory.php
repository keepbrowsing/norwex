<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\CustomerStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $randomCustomerStatus = CustomerStatus::all()->random(1)->first();
        return [
            'name' => $this->faker->name,
            'status_id' => $randomCustomerStatus->id,
        ];
    }
}
