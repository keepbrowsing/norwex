<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Get one random order status between three
        $randomStatus = Arr::random(
            [Order::TYPE_COMPLETED, Order::TYPE_PENDING, Order::TYPE_DELIVERED],
            1
        )[0];

        return [
            'order_status' => $randomStatus,
            'order_total' => $this->faker->randomFloat(2, 0, 2000),
            'created_at' => $this->faker->dateTimeBetween('-70 weeks', 'now'),
        ];
    }
}
